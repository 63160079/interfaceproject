/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.interfaceproject;

/**
 *
 * @author Rattanalak
 */
public abstract class Animal {
    private String name;
    private int numOfleg;

    public Animal(String name, int numOfleg) {
        this.name = name;
        this.numOfleg = numOfleg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumOfleg() {
        return numOfleg;
    }

    public void setNumOfleg(int numOfleg) {
        this.numOfleg = numOfleg;
    }

    @Override
    public String toString() {
        return "Animal{" + "name=" + name + ", numOfleg=" + numOfleg + '}';
    }
    
    public abstract void eat();
    public abstract void speak();
    public abstract void sleep();
}
