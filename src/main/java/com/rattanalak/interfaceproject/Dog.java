/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.interfaceproject;

/**
 *
 * @author Rattanalak
 */
public class Dog extends LandAnimal {

    public Dog() {
        super("Dog", 4);
    }

    @Override
    public void eat() {
       System.out.println("Dog : eat");
    }

    @Override
    public void speak() {
        System.out.println("Dog : speak >> box box!!!");
    }

    @Override
    public void sleep() {
        System.out.println("Dog : sleep");
    }

    @Override
    public void run() {
        System.out.println("Dog : run");
    }
    
}
