/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.interfaceproject;

/**
 *
 * @author Rattanalak
 */
public class Plane extends Vahicle implements Flyable,Runable{

    public Plane(String engine) {
        super(engine);
    }

    @Override
    public void startEngine() {
        System.out.println("Plane : Start engine"); 
    }

    @Override
    public void stopEngine() {
        System.out.println("Plane : Stop engine"); 
    }

    @Override
    public void raiseSpeed() {
        System.out.println("Plane : raise speed"); 
    }

    @Override
    public void applyBreak() {
        System.out.println("Plane : apply break");
    }

    @Override
    public void fly() {
        System.out.println("Plane : fly"); 
    }

    @Override
    public void run() {
        System.out.println("Plane : run");
    }
    
}
