/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.interfaceproject;

/**
 *
 * @author Rattanalak
 */
public class TestFlyable {
    public static void main(String[] args) {
        Bat bat = new Bat();
        Plane plane = new Plane("Engine number I");
        bat.fly();//Animal,Poultry,Flyable
        plane.fly();// Vahicle,Flyable,Runable
        Dog dog = new Dog();//Animal,LandAnimal,Runable
        Car car = new Car("Engine number II");
        newLine();

        Flyable[] flyables = {bat,plane};
        for(Flyable f : flyables){
            if(f instanceof Plane){
                Plane p = (Plane)f;
                p.startEngine();
                p.run();
            }
            f.fly();
            
        }
        newLine();
        Runable[] runable = {dog,plane,car};
        for(Runable r : runable){
           if(r instanceof Plane) {
               Plane p = (Plane)r;
                p.startEngine();
           }
           r.run();
        }
    }

    private static void newLine() {
        System.out.println("------------------------");
    }
}
